const filterBy = (arr, boolean) => {
  return arr.filter((item) => typeof item !== typeof boolean);
};

filterBy([1, 2, "str", false], false);
